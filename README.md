这个项目开坑的时候还是个初中生，当时主要还是满足自己用手机听歌的需要，有个暂停停止就算能用了。
现在高中放假了闲的没事干决定把这个项目重新捡起来，加了点实用的功能好歹也算是收个尾。在这之前从来没写过安卓、java啥的，整个都是边做边查，代码整个乱七八糟还是不要看的好。

## 特性

- 无缝无限循环
- 支持所有thtk能解包出`thbgm.fmt`、`musiccmt.txt`的版本（ogg版除外）
- 理论上效果跟游戏里Musicroom一毛一样
- 常见`musiccmt.txt`编码（汉化版等）自适应
- 耳机拔出自动暂停
- 音频占用自动暂停，占用完毕自动恢复（可关闭暂停）
- 单独设置音量
- 变速（改采样率实现的，所以会变调）
- 不可拖动的进度条。。audioTrack的api太神奇了搞不懂
- 长按介绍文本可以复制

另外本来打算做个通知栏控制的，结果好麻烦还是算了。
好看的界面啥的写不来，又不是不能用.jpg

---

### 截图：
![sh01](screenshot/1.jpg)
![sh02](screenshot/2.jpg)
![sh03](screenshot/3.jpg)
![sh04](screenshot/4.jpg)

### 下载

[RELEASES](https://gitee.com/wdsjimin/th-music-box/releases)

### 教程

请参考[thbgm-unpacker](https://gitee.com/wdsjimin/thbgm-unpack#faq)自行解包thxx.dat，然后将解出的`thbgm.fmt`、`musiccmt.txt`连同游戏根目录下的`thbgm.dat`传到手机后在app自行添加。