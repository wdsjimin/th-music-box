package com.zjkwdy.thBgm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Map;

public class editBgmGroup extends AppCompatActivity {

    private EditText name;
    private EditText fmt;
    private EditText dat;
    private EditText cmt;

    private AppJsonConfig jsonConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_bgm_group);
        this.name = findViewById(R.id.groupName);
        this.fmt = findViewById(R.id.fmtPath);
        this.dat = findViewById(R.id.datPath);
        this.cmt = findViewById(R.id.cmtPath);
        Intent intent = getIntent();
        jsonConfig = new AppJsonConfig(constConfig.CONFIG_FILE_PATH);
        if(intent.hasExtra(constConfig.EXTRA_EDT_GROUP)) {
            String groupName = intent.getStringExtra(constConfig.EXTRA_EDT_GROUP);
            Map<String,String> group = jsonConfig.getGroup(groupName);
            this.name.setText(groupName);
            this.dat.setText(group.get(AppJsonConfig.TAG_DAT_PATH));
            this.fmt.setText(group.get(AppJsonConfig.TAG_FMT_PATH));
            this.cmt.setText(group.get(AppJsonConfig.TAG_CMT_PATH));
        }
    }

    public void add(View view) {
        String nameStr, fmtStr, datStr, cmtStr;
        nameStr = this.name.getText().toString();
        fmtStr = this.fmt.getText().toString();
        datStr = this.dat.getText().toString();
        cmtStr = this.cmt.getText().toString();
        if (!(
                publicMethods.fileIsExists(datStr) &&
                publicMethods.fileIsExists(fmtStr)
        )||nameStr.equals("")) {
            Toast.makeText(this,R.string.fileNotfound,Toast.LENGTH_LONG).show();
            return;
        }
        jsonConfig.addGroup(nameStr, datStr, fmtStr, cmtStr);
        finish();
    }

//    private void commitEditor(SharedPreferences.Editor editor) {
//        editor.apply();
//        editor.commit();
//    }
//
//    public void addNewGroup(String groupName, String datPath, String fmtPath,String cmtPath) {
//        if (
//                publicMethods.fileIsExists(datPath) &&
//                publicMethods.fileIsExists(fmtPath) &&
//                publicMethods.fileIsExists(cmtPath)
//        ) {
//            SharedPreferences groups = getSharedPreferences("groups", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = groups.edit();
//            editor.putString(groupName, groupName);
//            commitEditor(editor);
//
//            SharedPreferences bgm = getSharedPreferences(groupName, Context.MODE_PRIVATE);
//            editor = bgm.edit();
//            editor.putString("fmt", fmtPath);
//            editor.putString("dat", datPath);
//            editor.putString("cmt", cmtPath);
//            commitEditor(editor);
//            Toast.makeText(this, groupName + "Add Success!", Toast.LENGTH_LONG).show();
//            finish();
//        } else {
//            Toast.makeText(this, "Input File NotFound", Toast.LENGTH_LONG).show();
//        }
//    }
}