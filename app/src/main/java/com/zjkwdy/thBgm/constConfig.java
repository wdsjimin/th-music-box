package com.zjkwdy.thBgm;

public class constConfig {

    public static final String EXTRA_DAT_NAME = "com.zjkwdy.demoapp.datName";
    public static final String EXTRA_FMT_NAME = "com.zjkwdy.demoapp.fmtName";
    public static final String EXTRA_CMT_NAME = "com.zjkwdy.demoapp.cmtName";
    public static final String EXTRA_GRP_NAME = "com.zjkwdy.demoapp.grpName";
//    public static final String EXTRA_EDT_MODE = "com.zjkwdy.demoapp.editMode";
    public static final String EXTRA_EDT_GROUP = "com.zjkwdy.demoapp.editGroup";

    public static final String CONFIG_FILE_PATH = "/storage/emulated/0/THPlayer/constConfig.json";
    public static final String[] TRY_CMT_FILE_CHARSETS = {"Shift-JIS","GB2312","GBK","GB18030","UTF-8"};
}
