package com.zjkwdy.thBgm;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import java.io.IOException;

public class thbgm {
    public final String name;
    public final Integer startTime;
    public final Integer durTime;
    public final Integer loopStart;
    private final Integer loopStartInFrames;
    private final Integer loopDurInFrames;
    public final Integer Sample;
    public final Integer channels;
    public final Integer bits;
    public final Integer frameSize;
    public final Integer frames;
    public Float speed;
    public Float volume;
    public Thread playThread;
    public volatile AudioTrack audioTrack;

    public thbgm(String name, int startTime, int durTime, int loopStart, int sample, int channels, int bits/*,int byteRate*/) {
        this.name = name;
        this.startTime = startTime;
        this.durTime = durTime;
        this.loopStart = loopStart;
        this.Sample = sample;
        this.channels = channels;
        this.bits = bits;
        this.frameSize = channels*(bits/8);
        this.frames = durTime / frameSize;
        this.loopStartInFrames = loopStart / frameSize;
        this.loopDurInFrames = this.frames - this.loopStartInFrames;
        this.speed = (float) 1.0;
        this.volume = (float) 1.0;
    }

    public void stop() {
        assert this.playThread != null;
        this.playThread = null;
        this.audioTrack.stop();
        this.audioTrack.release();
    }

    public void pause() {
        assert this.audioTrack != null;
        this.audioTrack.pause();
    }

    public void setSpeed(float speed) {
        this.speed = speed;
        if (this.audioTrack != null)
            this.audioTrack.setPlaybackRate(Math.round(Sample * speed));
    }

    public void setVol(float vol) {
        this.volume = vol;
        if (this.audioTrack != null)
            this.audioTrack.setVolume(vol);/*            Log.d("TIME",Integer.toString(getNowPercent()));*/
    }

    public int getNowPercent() {
        int now = this.getNowFrames();
        if (now > this.loopStartInFrames)
            now = loopStartInFrames + (now - this.loopStartInFrames) % this.loopDurInFrames;
        return Math.round((((float) now * frameSize / durTime) % 1) * 100);
    }

    public int getNowFrames() {
        if (this.audioTrack == null) return 0;
        return this.audioTrack.getPlaybackHeadPosition();
    }

    public void setPlayThread(bgmdat dat) {
        this.playThread = new Thread(() -> {
            try {
                if (audioTrack == null || audioTrack.getState() != AudioTrack.STATE_INITIALIZED) {
                    audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, Math.round(Sample * speed), AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, durTime, AudioTrack.MODE_STATIC);
                    audioTrack.setVolume(volume);
                    dat.seek(startTime);
                    byte[] musicBytes = dat.read(durTime);
                    audioTrack.write(musicBytes, 0, durTime);
                    audioTrack.setLoopPoints(loopStart / frameSize, frames, -1);
                }
                audioTrack.play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.playThread.start();
    }
}
