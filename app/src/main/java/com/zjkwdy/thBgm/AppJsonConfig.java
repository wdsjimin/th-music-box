package com.zjkwdy.thBgm;

import static com.zjkwdy.thBgm.publicMethods.readJsonFile;
import static com.zjkwdy.thBgm.publicMethods.writeStringToFile;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AppJsonConfig {
    public JSONObject rawJsonConfig;
    public final String path;

    public static final String TAG_GROUPS = "groups";
    public static final String TAG_DAT_PATH = "dat";
    public static final String TAG_FMT_PATH = "fmt";
    public static final String TAG_CMT_PATH = "cmt";

    public AppJsonConfig(String path) {
        this.path = path;
        try {
            if(!publicMethods.fileIsExists(path)) makeConfig();
            else rawJsonConfig = readJsonFile(path, StandardCharsets.UTF_8);
        } catch (Exception ignore) {}
    }

    private void makeConfig(){
        try {
            rawJsonConfig = new JSONObject(){{
                put(TAG_GROUPS,new JSONObject());
            }};
            writeToFile();
        } catch (Exception ignored) {}
    }

    private void writeToFile() throws IOException {
        writeStringToFile(this.path,rawJsonConfig.toString());
    }

    public Map<String, Map<String,String>> getGroupList() {
        Map<String,Map<String,String>> tmp = new HashMap<>();
        try{
            JSONObject o = rawJsonConfig.getJSONObject(TAG_GROUPS);

            Iterator<?> it = o.keys();
            while (it.hasNext()) {
//                Map.Entry<String,> entry = (Map.Entry<String, Object>) it.next();
//                tmp.put(entry.getKey(),entry.getValue());
                String key = (String) it.next();
                tmp.put(key,new HashMap<>(){{
                    put(TAG_DAT_PATH,o.getJSONObject(key).getString(TAG_DAT_PATH));
                    put(TAG_FMT_PATH,o.getJSONObject(key).getString(TAG_FMT_PATH));
                    put(TAG_CMT_PATH,o.getJSONObject(key).getString(TAG_CMT_PATH));
                }});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp;
    }

    public void addGroup(String groupName,String datPath,String fmtPath,String cmtPath) {
        try {
            rawJsonConfig.getJSONObject(TAG_GROUPS).put(groupName,new JSONObject(){{
                put(TAG_DAT_PATH,datPath);
                put(TAG_FMT_PATH,fmtPath);
                put(TAG_CMT_PATH,cmtPath);
            }});
            writeToFile();
        } catch (Exception ignore) {}
    }

    public void delGroup(String groupName) {
        try {
            rawJsonConfig.getJSONObject(TAG_GROUPS).remove(groupName);
            writeToFile();
        } catch (Exception ignore) {}
    }

    public Map<String,String> getGroup(String groupName) {
        Map<String,String> tmp = new HashMap<>();
        try {
            JSONObject o = rawJsonConfig.getJSONObject(TAG_GROUPS).getJSONObject(groupName);
            tmp.put(TAG_DAT_PATH,o.getString(TAG_DAT_PATH));
            tmp.put(TAG_FMT_PATH,o.getString(TAG_FMT_PATH));
            tmp.put(TAG_CMT_PATH,o.getString(TAG_CMT_PATH));
        } catch (Exception ignored){}
        return tmp;
    }

}
