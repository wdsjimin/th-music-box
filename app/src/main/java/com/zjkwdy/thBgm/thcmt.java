package com.zjkwdy.thBgm;

import static com.zjkwdy.thBgm.publicMethods.readTextFile;
import static com.zjkwdy.thBgm.constConfig.TRY_CMT_FILE_CHARSETS;


import android.util.Log;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class thcmt {
    final Map<String, String> cmtMap;

    public thcmt(String fileName) {
        this.cmtMap = new HashMap<>();
        String result = "none";
        for (String charset : TRY_CMT_FILE_CHARSETS) {
            try {
                result = readTextFile(fileName, Charset.forName(charset));
                Log.d("THCMT", "target Charset:" + charset);
                break;
            } catch (IOException ignored) {}
        }
        String[] arr = result.split("@bgm/");
        for (String s : arr) {
            if (s.startsWith("#")) continue;
            String[] cmtInLines = s.split("\n");
            String bgm_name = cmtInLines[0];
            /*Log.d("CMT "+bgm_name.split("\\.")[0],arr[i]);*/
            this.cmtMap.put(bgm_name.split("\\.")[0], s);
        }
    } /* @params bgmId a string like th13_01 */

    public String getCMT(String bgmId) {
        if (this.cmtMap == null || this.cmtMap.isEmpty() || !this.cmtMap.containsKey(bgmId))
            return "404 音乐幻想入";
        return this.cmtMap.get(bgmId);
    }
}
