package com.zjkwdy.thBgm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private Spinner groupChoose;
    private Button goBtn;
    private ImageButton delBtn;
    private ImageButton edtBtn;

    private AppJsonConfig jsonConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        publicMethods.verifyPermission(getApplicationContext(), this);
        this.groupChoose = findViewById(R.id.groupChoose);
        this.goBtn = findViewById(R.id.go);
        this.delBtn = findViewById(R.id.delGroupBtn);
        this.edtBtn = findViewById(R.id.editGroupBtn);

        flushGroups();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //Log.d("OnRestart","On-restart");
        flushGroups();
    }

    public void add(View view) {
        Intent intent = new Intent(this, editBgmGroup.class);
        startActivity(intent);
    }

    public void editGroup(View view) {
        String selectGroup = this.groupChoose.getSelectedItem().toString();
        Intent intent = new Intent(this, editBgmGroup.class);
//        intent.putExtra(constConfig.EXTRA_EDT_MODE,true);
        intent.putExtra(constConfig.EXTRA_EDT_GROUP,selectGroup);
        startActivity(intent);
    }

    public void flushGroups() {
        jsonConfig = new AppJsonConfig(constConfig.CONFIG_FILE_PATH);
        Map<String, Map<String, String>> sp = jsonConfig.getGroupList();
        Set<String> allEntries = sp.keySet();
        String[] groups;
        if (allEntries.size() == 0) {
            this.goBtn.setEnabled(false);
            this.delBtn.setEnabled(false);
            this.edtBtn.setEnabled(false);
            groups = new String[]{"啥也没有"};
        } else {
            this.goBtn.setEnabled(true);
            this.delBtn.setEnabled(true);
            this.edtBtn.setEnabled(true);
            groups = allEntries.toArray(new String[0]);
        }
        ArrayAdapter<String> adapterNames = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, groups);
        groupChoose.setAdapter(adapterNames);
    }

    public void delGroup(View view) {
        String selectGroup = this.groupChoose.getSelectedItem().toString();
        jsonConfig.delGroup(selectGroup);
        flushGroups();
    }


    public void processBGM(View view) {
        String selectGroup = this.groupChoose.getSelectedItem().toString();
        Map<String,String> group = jsonConfig.getGroup(selectGroup);
        Intent intent = new Intent(this, BgmPlayActivity.class);
        intent.putExtra(constConfig.EXTRA_DAT_NAME, group.get(AppJsonConfig.TAG_DAT_PATH));
        intent.putExtra(constConfig.EXTRA_FMT_NAME, group.get(AppJsonConfig.TAG_FMT_PATH));
        intent.putExtra(constConfig.EXTRA_CMT_NAME, group.get(AppJsonConfig.TAG_CMT_PATH));
        intent.putExtra(constConfig.EXTRA_GRP_NAME, selectGroup);
        startActivity(intent);
    }
}